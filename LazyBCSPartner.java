//estruturas de pastas no projeto (Packages)
package org.sagui.chksim.algorithm.qs;
// utiliza-se o serializable pq é meio que padrão para salvamento de estados no java em banco de dados
// principalmente quando são diferentes, porém se não for bem aplicado causa quebra de segurança do algoritimo
import java.io.Serializable;
import org.sagui.chksim.QuasiSynchronousAlgorithm;
// Ao inserir o algorítmo no simulador é preciso gerar um um novo jar porque o jar gerado não reconhece o novo alg inserido
public class LazyBCSPartner extends QuasiSynchronousAlgorithm {
  private int lc;
  private boolean equiv;
  private int partner;
    private int [] dv;//vetor de inteiros
    private boolean [] simple;//vetor de booleanos
    protected final int NO_PARTNER = 0;
    protected final int MORE_THAN_ONE_PARTNER = 2;

    //toda parte inicial que executa uma vez apenas, consultor da classe
    protected void doInit() {
      lc = 0;
        //para todo i -> d[i] = 0
      dv = new int[getProcessNumber()];
      for(int i = 0; i<dv.length; i++){
        dv[i] = 0;
      } 
	//para todo simple[i] = false
      for(int i = 0; i<simple.length; i++){
        simple[i] = false;
      }
     //dv[getProcess()]
      dv[getProcess()]++;
      simple[getProcess()] = true;
      partner = NO_PARTNER;
      equiv = true;
	//armazena o checkpoint inicial
      takeBasicCheckpoint();
    }

    //checkpointing basico
    protected void doTakeBasicCheckpoint() {
      if (!equiv) {
        lc++;
        equiv = true;
    }// "reset" do simple após o checkpointing basico
    dv[getProcess()]++;
    for(int i = 0; i<simple.length; i++){
      simple[i] = false;
    }
     //no_partner serve como identificador mesmo ... pode ser atribuido 0,1,2
    partner = NO_PARTNER;
	 //armazena o checkpoint basico
    takeBasicCheckpoint();
  }

    //é chamado pelo simulador e envia um mensagem, simula um envio para um processo qualquer
  protected Serializable doSendMessage(int destination) {
   if(partner == NO_PARTNER){
    partner = destination;
  } else{
   partner = MORE_THAN_ONE_PARTNER;
 }

 return new Mensagem(lc,simple[destination],dv[destination],dv[getProcess()]);
}

// é chamado pelo simulador para a simulação de recebimento da mensagem serializavel com checkpointing forçado
@Override
protected void doReceiveMessage(int sender, Serializable message) {
  Mensagem m = (Mensagem) message;
  int destination =  getProcessNumber();
  if(m.messageLC > lc && partner != NO_PARTNER) {
   if(partner != destination || (partner == destination && m.dvR == dv[getProcess()] && !m.simpleR)){
    dv[getProcess()]++;
    for(int i = 0; i<simple.length; i++){
      simple[i] = false;
    }
    simple[getProcess()] = true;
    partner = NO_PARTNER;
  }
	//metodo checkpoint forçado
  takeForcedCheckpoint();
}
if (m.messageLC == lc) {
  equiv = false;
}
lc = Integer.max(lc, m.messageLC);
if(m.dvS > dv[destination]){
  dv[destination] = m.dvS;
  simple[destination] = true;
}
receiveMessage(m.dvS, m);
}

public class Mensagem implements Serializable{
  public int messageLC;
  public boolean simpleR;
  public int dvR;
  public int dvS;

  public Mensagem(int lc, boolean simple, int dvS, int dvR) {
    this.messageLC = lc;
    this.simpleR = simple;
    this.dvS = dvS;
    this.dvR = dvR;
  }
}
}


